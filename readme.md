# UEditorPlus 使用文档

UEditorPlus 使用文档 [https://open-doc.modstart.com/ueditor-plus/](https://open-doc.modstart.com/ueditor-plus/) 源码仓库。

我们会定期同步发布到官方文档中心。

## 开发贡献

**安装nodejs** 

版本要求：`>=14`

**初始化**

```
npm install
npm install -g vuepress
```

**开发**

```
npm run dev
```


## 授权协议

本项目采用知识共享 (Creative Commons) 署名—非商业性使用—禁止演绎 4.0公共许可协议国际版（CC BY-NC-ND 4.0）

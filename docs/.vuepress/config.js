const path = require('path')

module.exports = {
    base: '/ueditor-plus/',
    env: {
        browser: true,
        node: true,
    },
    head: [
        ['link', {rel: 'shortcut icon', type: "image/x-icon", href: `/favicon.ico`}],
        ['script', {}, `var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?f84f35a44b5cc5c0b10c3fabdf0f322b";
  var s = document.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(hm, s);
})();`],
    ],
    locales: {
        '/': {
            lang: '简体中文',
            title: 'UEditorPlus 使用文档',
            description: '基于 UEditor 二次开发的富文本编辑器'
        },
        // '/en/': {
        //     lang: 'English',
        //     title: 'ModStart',
        //     description: 'A Laravel Quick Develop Framework'
        // }
    },
    themeConfig: {
        nav: [
            {text: "首页", link: "/",},
            {text: '介绍', link: '/guide.html'},
            {text: '配置', link: '/manual.html'},
            {text: '教程',  items: [
                {text: '公式使用教程', link: '/formula.html'},
            ]},
            {text: '后端部署', link: '/backend.html'},
            {text: '常见问题', link: '/qa.html'},
            {text: "更新日志", link: "/change-log.html",},
            {text: "ModStart框架", link: "https://modstart.com",},
        ],
        sidebar: 'auto',
        sidebarDepth: 2,
    },
    markdown: {
        lineNumbers: true
    },
    plugins: [
        [
            '@vuepress/back-to-top'
        ],
        [
            '@vuepress/last-updated',
            {
                transformer: (timestamp, lang) => {
                    const moment = require('moment')
                    moment.locale(lang)
                    return moment(timestamp).fromNow()
                }
            }
        ]
    ],
    configureWebpack: () => {
        const NODE_ENV = process.env.NODE_ENV
        console.log('hello', NODE_ENV)
        if (NODE_ENV === 'production') {
            return {
                output: {
                    publicPath: 'https://open-doc-cdn.modstart.com/ueditor-plus/'
                },
            }
        } else {
            return {}
        }
    }
}

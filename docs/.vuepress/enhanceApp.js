import 'markdown-it-vue/dist/markdown-it-vue.css'

export default ({Vue,router}) => {
    router.beforeEach((to, from, next) => {
        if (typeof _hmt != "undefined") {
            if (to.path) {
                const path = `/ueditor-plus/${to.fullPath}`
                _hmt.push(["_trackPageview", path]);
            }
        }
        next();
    });
};

# UEditorPlus 更新日志

## v3.9.0 Vue/React示例，返回参数统一封装，已知问题修复

- 新增：vue2、vue3、react 使用示例
- 新增：支持 serverResponsePrepare 参数，支持返回统一处理 [#gitee-I7XI5Z](https://gitee.com/modstart-lib/ueditor-plus/issues/I7XI5Z)
- 新增：导入内容 js 文件本地化，更好的支持本地化部署
- 优化：单图上传自动筛选格式 [#gitee-I9GMK8](https://gitee.com/modstart-lib/ueditor-plus/issues/I9GMK8)
- 修复：开启图片压缩，图片压缩失败的情况下继续上传
- 修复：文件超出设定大小 webuploader 异常问题 [#gitee-I8F0XT](https://gitee.com/modstart-lib/ueditor-plus/issues/I8F0XT) [#gitee-I87YQH](https://gitee.com/modstart-lib/ueditor-plus/issues/I87YQH)

## v3.8.0 文档导入支持直接粘贴 Markdown 格式，已知问题修复

- 新增：文档导入支持直接粘贴 Markdown 格式
- 新增：clear 方法，支持清空编辑器内容
- 修复：reset 方法调用无效问题
- 修复：表单 form 中会自动生成重复 textarea 问题修复 [#gitee-I9038G](https://gitee.com/modstart-lib/ueditor-plus/issues/I9038G)
- 修复：contentchange 监听中文输入有误问题修复 [#gitee-I8RZJD](https://gitee.com/modstart-lib/ueditor-plus/issues/I8RZJD)
- 修复：音频、视频、文件上传增加自定义头部信息 [#gitee-I8ODVU](https://gitee.com/modstart-lib/ueditor-plus/issues/I8ODVU)
- 修复：多编辑器会出现跨编辑快捷键操作异常问题 [#gitee-I8H504](https://gitee.com/modstart-lib/ueditor-plus/issues/I8H504)
- 优化：统一配置请求的URL [#gitee-I8W7FY](https://gitee.com/modstart-lib/ueditor-plus/issues/I8W7FY)


## v3.7.0 Word、Markdown文档一键导入，部分问题修复

- 新增：支持文档一键导入，支持Word文档（docx）、Markdown文档（md）文档的导入
- 新增：gitee issue 提交模板配置文件
- 修复：多样式文件 iframeCssUrlsAddition 引入 Bug [#gitee-I8KM4H](https://gitee.com/modstart-lib/ueditor-plus/issues/I8KM4H)
- 修复：simpleupload 的 toolbarCallback 无法拦截 [#gitee-I8EGOA](https://gitee.com/modstart-lib/ueditor-plus/issues/I8EGOA)
- 修复：CDN链接调整 browser-image-compression.min.js [#gitee-I82LF7](https://gitee.com/modstart-lib/ueditor-plus/issues/I82LF7)

## v3.6.0 图标补全，精简代码，快捷操作重构，问题修复

- 新增：补全文件图标，文件显示更丰富
- 新增：获取文档顶部和左侧偏移量 `getOffsetTop` `getOffsetLeft`
- 优化：list 无用代码优化，所有代码格式化
- 优化：公式编辑多语言显示优化
- 优化：静态文件打包时增加清除缓存时间戳
- 优化：快捷菜单 `ShortCutMenu` 部分重构
- 修复：字体设定后再次查询异常问题
- 修复：图片包裹边线、图片编辑菜单超出编辑区异常 [#gitee-I7HX2U](https://gitee.com/modstart-lib/ueditor-plus/issues/I7HX2U)

## v3.5.0 支持音频组件，字体图标请求合并，服务器配置优化

- 新增：支持音频插入组件，支持MP3
- 优化：弹窗非缩放模式下高度显示异常问题
- 优化：服务器返回配置和本地配置合并方式采用deep merge方式
- 优化：字体图标使用base64编码优化请求数量

## v3.4.0 全新公式编辑体验，Logo全新发布

- 新增：公式在线编辑支持，支持可视化和专业版
- 新增：公式编辑支持 live 和 plain 双模式
- 新增：WORD图片转存支持多张图片同时转存
- 新增：软件全新Logo发布
- 优化：公式渲染默认替换为LatexEasy引擎
- 修复：图片上传文件名丢失问题

## v3.3.0 图片上传压缩重构，UI优化，升级基础组件

- 新增：升级webuploader到最新版本
- 新增：引入axios、browser-image-compression，渐进完成接口图片处理
- 新增：完成simpleimage图片上传和上传压缩重构
- 优化：富文本菜单和快捷操作UI样式优化
- 优化：底部状态栏文字颜色统一调整
- 优化：快捷操作栏默认操作内容调整优化
- 修复：源码模式下 contentchange 时间不触发问题

## v3.2.0 兼容规则过滤，若干问题修复

- 新增：清除格式时，默认移除Table、List上的样式
- 新增：增加CORS_URL兼容处理 [#gitee-I6V75H](https://gitee.com/modstart-lib/ueditor-plus/issues/I6V75H)
- 修复：输入第一个字符，contentchange事件监听不到 [#gitee-I7EQZM](https://gitee.com/modstart-lib/ueditor-plus/issues/I7EQZM)
- 修复：Word转存路径URL转码问题 [#gitee-I69ER3](https://gitee.com/modstart-lib/ueditor-plus/issues/I69ER3)
- 修复：多个ueditor同时显示时，shortcutmenu报错 [#gitee-I69WNW](https://gitee.com/modstart-lib/ueditor-plus/issues/I69WNW)
- 修复：编辑器Section滚动到视图问题修复 [#gitee-I6LTOZ](https://gitee.com/modstart-lib/ueditor-plus/issues/I6LTOZ)

## v3.1.0 支持CSS变量，支持Mind代码类型，若干问题修复

- 新增：支持使用CSS变量 `--ueditor-top-offset` 设定全屏模式下顶部偏移量
- 优化：Markdown快捷键屏蔽非Chrome浏览器
- 优化：默认内容操作快捷菜单按钮显示优化
- 优化：双击在内容后追加空行信息，避免最后一个节点为非文字元素难以换行问题
- 优化：代码预览类型，增加Mind代码类型
- 修复：图片上传未携带请求头信息 [#gitee-I6T18P](https://gitee.com/modstart-lib/ueditor-plus/issues/I6T18P)
- 修复：MarkDown标题快捷键失效问题

## v3.0.0 接口请求头参数，插入换行优化，若干问题优化

- 新增：Markdown标题快捷模式（输入多个#+空格自动格式化为标题）
- 新增：当内容最末尾为非字符时，比较难以在最后插入字符问题
- 新增：beforesubmit事件，方便提交前的数据处理
- 新增：浏览自高度和宽度自动变化时候增加动画效果
- 新增：后端接口新增公共头参数 [#gitee-I6KK5D](https://gitee.com/modstart-lib/ueditor-plus/issues/I6KK5D)
- 新增：默认增加 message 插件，支持消息提示
- 优化：字体设定和字体选择，支持恢复默认字体和字体不存在时异常显示 [#gitee-I6JHV4](https://gitee.com/modstart-lib/ueditor-plus/issues/I6JHV4)
- 优化：富文本编辑器UEditor样式缓存问题
- 优化：草稿功能数据恢复逻辑优化，避免初始内容覆盖保存值问题
- 优化：补全VUE使用文档示例配置项，避免CROS跨域引起的问题
- 修复：源码中默认增加两次，生成的all.js 图片等操作多一次默认 [#gitee-I6L6LU](https://gitee.com/modstart-lib/ueditor-plus/issues/I6L6LU)

## v2.9.0 文档仓库开源，修复若干问题

- 新增：`UEditorPlus`
  文档开源 [https://gitee.com/modstart-lib/ueditor-plus-doc](https://gitee.com/modstart-lib/ueditor-plus-doc)
- 修复：插入视频中对齐方式 `默认` 按钮的 `title` 属性为 `undefined` 的问题 [#gitee-2](https://gitee.com/modstart-lib/ueditor-plus/pulls/2)
- 修复：`catchremoteimage.js` 中缺少加载中图片变量的问题 [#github-4](https://github.com/modstart-lib/ueditor-plus/pull/4)
- 修复：部分文字错误拼写问题，完善部分注释

## v2.8.0 颜色自定义，文档功能完善

- 新增：单图上传时也对图片进行压缩处理，根据配置自动切换压缩和非压缩模式
- 新增：颜色选择框新增自定义颜色选择组件（现代浏览器支持）
- 新增：服务器部署文档完善和常见问题说明文档
- 优化：自动格式化按钮样式显示优化
- 优化：颜色选择框排版样式间距调整

## v2.7.0 开放独立文档，附件样式优化

- 新增：开放 `UEditorPlus` 使用文档独立站
- 优化：优化 `OSX` 系统编辑器字体显示问题
- 优化：附件显示样式调整，图标替换为SVG
- 优化：源码模式下光标显示错位问题优化
- 修复：源码模式下内容编辑不生效问题修复

## v2.6.0 编辑器草稿功能，操作体验优化

- 新增：自动保存功能，新增 `autoSaveEnable`, `autoSaveRestore`, `autoSaveKey` 配置
- 新增：从草稿箱恢复到浏览器 `auto_safe_restore` 命令
- 优化：重构 `localStorage` 部分代码，适配现代浏览器
- 优化：重构完整演示 `Demo` 功能部分，所有测试功能重构
- 修复：图片高度过高时操作浮窗显示问题 [issue](https://gitee.com/modstart-lib/ueditor-plus/issues/I5TXOX)

## v2.5.0 Latex公式编辑，源码样式优化

- 新增：公式编辑器功能，提供latex语法的公式编辑器
- 优化：Word图片转存标识调整为data-word-image
- 优化：富文本编辑器浮层弹出按钮显示逻辑和样式优化
- 优化：系统集成时源代码编辑行距问题优化
- 优化：仓库新增Makefile配置文件方便快速构建

## v2.4.0 Word图片粘贴重构，功能样式优化

- 新增：Word粘贴内容图片转存wordimage功能重构
- 新增：引入第三方复制插件clipboard库
- 新增：转存图片新增默认点击事件弹出转存弹窗
- 优化：多图上传并发数调整为1保证上传顺序
- 优化：弹窗按钮样式错位显示优化
- 优化：Word图片本地转存占位图优化
- 优化：删除Flash相关无用过时组件

## v2.3.0 图片抓取重构，多处样式优化

- 新增：自动抓取图片优化为串行抓取，避免批量接口超时问题
- 新增：自定义菜单按钮样式类 edui-toolbar-button-custom
- 移除：移除百度地图插件
- 优化：文件粘贴上传Loading样式美化，勾选和单选基础颜色调整
- 优化：颜色选择工具颜色条样式错位调整
- 优化：工具栏下拉采样样式优化，页面margin导致的下拉错位
- 优化：演示Demo中自定义标题下拉样式

## v2.2.0 vue示例支持，图片尺寸设定异常修复

- 新增：Dom 操作添加 _propertyDelete 属性，方便删除属性
- 新增：图片编辑宽高为空时自动清除图片宽度和高度
- 新增：vue使用示例说明（需第三方库支持）
- 修复：编辑器只包含视频，提交到服务器端的内容为空
- 优化：移除 video parse 无用插件

## v2.1.0 演示网站重构，浮动工具和表格双击优化

- 新增：新增unsetFloating方法，方便动态Editor浮动工具栏处理
- 优化：表格边框双击时间调整为200ms（解决拖拽延迟问题）
- 优化：重新整理Demo页面也代码
- 修复：右击菜单图标和工具栏菜单冲突问题

## v2.0.0 让UEditor重新焕发活力

- 优化：优化界面样式，使用字体图标
- 新增：setWidth方法，可设置编辑器宽度
- 新增：视频和图片上传参数（见 ueditor.config.js 配置文件）
- 新增：toolbarCallback 属性，可以自定义响应工具栏图标点击
- 移除：谷歌地图、图片搜索、音乐搜索、截屏
